//images .svg
import Logo from '../Images/logo.svg';
import Offline from '../Images/offline.svg';

export const Images = {
  // svgs
  Logo,
  Offline,
};
