import {createContext, ReactNode, useEffect, useState} from 'react';
import NetInfo, {useNetInfo} from '@react-native-community/netinfo';
import axios from 'axios';
//Types
import {GlobalParamsState, GlobalParamsValues} from './Types';

const GlobalParamsContext = createContext<GlobalParamsValues>({
  serverIsDown: false,
  isConnected: true,
  loadingApp: false,
  setServerState: async () => {},
});

interface Props {
  children: ReactNode;
}

export const ProviderGlobalParams: React.FC<Props> = props => {
  //state
  const [state, setState] = useState<GlobalParamsState>({
    loadingApp: true,
    isConnected: true,
    serverIsDown: false,
  });
  const netInfo = useNetInfo();
  const isConnected = netInfo.isConnected && netInfo.isInternetReachable;

  //useEffect
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      setState(prev => ({...prev, isConnected: state.isConnected || true}));
    });
    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    (async () => {
      setState(prevState => ({...prevState, loadingApp: true}));
      try {
        await setServerState();
        //getting token and the initial data for the app before showing the actual navigation stack
        setTimeout(
          () =>
            setState(prevState => ({
              ...prevState,
              loadingApp: false,
            })),
          2000,
        );
      } catch (error) {
        setState(prevState => ({...prevState, loadingApp: false}));
      }
    })();
  }, []);

  // Functions
  const setServerState = async () => {
    axios.get('https://dummyapi.io/data/v1/user').catch(error => {
      if (!error?.response?.status) {
        setState(prev => ({
          ...prev,
          serverIsDown: true,
        }));
      } else {
        setState(prev => ({
          ...prev,
          serverIsDown: false,
        }));
      }
    });
  };

  //Render
  return (
    <GlobalParamsContext.Provider
      value={{
        ...state,
        isConnected: typeof isConnected === 'boolean' ? isConnected : true,
        setServerState,
      }}>
      {props.children}
    </GlobalParamsContext.Provider>
  );
};

export {GlobalParamsContext};
