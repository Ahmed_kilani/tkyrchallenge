export type GlobalParamsState = {
  isConnected: boolean;
  loadingApp: boolean;
  serverIsDown: boolean;
};

export interface GlobalParamsValues extends GlobalParamsState {
  setServerState: () => Promise<void>;
}
