import {FC, memo, useEffect} from 'react';
import {Platform, StatusBar} from 'react-native';
//Components
import {Box} from '../../Components/Commons';
//Constants
import {Images} from '../../Constants';

const Splash: FC = () => {
  useEffect(() => {
    if (Platform.OS === 'android') {
      StatusBar.setHidden(true, 'slide');
    }
    return () => {
      if (Platform.OS === 'android') {
        StatusBar.setHidden(false, 'slide');
      }
    };
  }, []);

  return (
    <Box flex={1} background="#222850" content="center" items="center">
      <Images.Logo width={128} height={140} />
    </Box>
  );
};

export default memo(Splash);
