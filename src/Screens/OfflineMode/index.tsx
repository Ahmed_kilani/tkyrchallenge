import {FC, memo, useContext, useState} from 'react';
import {StyleSheet, TouchableOpacity, useWindowDimensions} from 'react-native';
//ContextApi
import {GlobalParamsContext} from '../../ContextApi';
//Components
import {Box, Text} from '../../Components/Commons';
// Constants
import {Images} from '../../Constants';

const OfflineMode: FC = () => {
  const {serverIsDown, setServerState} = useContext(GlobalParamsContext);
  const [loader, setLoader] = useState<boolean>(false);
  const {width, height} = useWindowDimensions();
  //Api

  const onPressRefrech = async () => {
    setLoader(true);
    setTimeout(() => setServerState().then(() => setLoader(false)), 2000);
  };

  return (
    <Box flex={1} content="space-evenly" items="center">
      <Box height={height * 0.3} content="space-around" items="center">
        <Text fontSize={30} fontWeight="700">
          {serverIsDown ? 'Server Is Down' : 'No Internet Connection'}
        </Text>

        <Box width={width * 0.6} items="center">
          <Text textAlign="center">
            {serverIsDown
              ? 'Due to maintenance.'
              : 'Please check your internet connection and try again.'}
          </Text>
        </Box>
      </Box>
      <Images.Offline width={128} height={140} />
      {serverIsDown && (
        <TouchableOpacity
          disabled={loader}
          style={styles.button}
          onPress={onPressRefrech}>
          <Text>Try Again</Text>
        </TouchableOpacity>
        // <Button
        //   text=""
        //   width="70%"
        //   borderWidth={1}
        //   loading={loader}
        //   onPress={onPressRefrech}
        // />
      )}
    </Box>
  );
};

export default memo(OfflineMode);

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
    width: '60%',
  },
});
