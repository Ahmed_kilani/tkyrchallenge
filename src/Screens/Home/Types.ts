import {RouteProp} from '@react-navigation/core';
import {
  AppNavigatorParamList,
  AppNavigatorProps,
} from '../../Navigations/AppNavigator/Types';

export type HomeNavigatorProps = {
  navigation: AppNavigatorProps;
  route: RouteProp<AppNavigatorParamList, 'Home'>;
};

export type UsersType = {
  id?: string;
  title?: string;
  firstName?: string;
  lastName?: string;
  picture?: string;
};
