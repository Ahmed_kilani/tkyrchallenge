import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import Animated from 'react-native-reanimated';
import {memo, useContext, useEffect, useState} from 'react';
import axios from 'axios';
// Components
import {Box, Spacer, Text} from '../../Components/Commons';
// ContextApi
import {GlobalParamsContext} from '../../ContextApi';
// Types
import {HomeNavigatorProps, UsersType} from './Types';
// Utils
import {sharedTransactions} from '../../Utils/CustomAnimations';

const PAGE_LIMIT = 10;

const Home: React.FC<HomeNavigatorProps> = props => {
  const {navigation} = props;
  const {setServerState} = useContext(GlobalParamsContext);
  const {width} = useWindowDimensions();

  //states
  const [loading, setLoading] = useState<boolean>(false);
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [page, setPage] = useState<number>(0);
  const [list, setList] = useState<UsersType[]>([]);

  //functions
  const onPressItem = (item: UsersType) => {
    navigation.navigate('ProductDetails', {user: item});
  };
  const onPressGenerate = () => {
    setLoading(true);
    setPage(1);
  };

  const onEndReached = () => {
    if (!loading && page > 0) {
      setPage(page + 1);
    }
  };

  const onRefresh = () => {
    setPage(1);
  };

  //useEffect
  useEffect(() => {
    if (page) {
      setLoading(true);
      axios
        .get(`https://dummyapi.io/data/v1/user`, {
          headers: {
            'app-id': '6505da4241d789a9d7dad561',
          },
          params: {
            page: page,
            limit: PAGE_LIMIT,
          },
        })
        .then(res => {
          if (page === 1) {
            setList(res.data.data);
          } else {
            setList([...list, ...res.data.data]);
          }
        })
        .catch(error => {
          console.error(error);

          if (!error?.response?.status) {
            setServerState();
          }
        })
        .finally(() => {
          if (page > 0) {
            setTimeout(() => setLoading(false), 2000);
          } else {
            setLoading(false);
          }
          setRefreshing(false);
        });
    }
  }, [page]);

  //render
  const renderItem = ({item, index}: {item: UsersType; index: number}) => {
    return (
      <TouchableOpacity key={index} onPress={() => onPressItem(item)}>
        <Animated.Image
          sharedTransitionTag={item.id}
          // sharedTransitionStyle={sharedTransactions}
          source={{uri: item.picture || ''}}
          style={[styles.image, {width: width * 0.4, height: width * 0.4}]}
        />
      </TouchableOpacity>
    );
  };

  const renderFooter = () => {
    if (loading) {
      return (
        <Box pTop={20} pBottom={20}>
          <ActivityIndicator size="large" color="#222850" />
        </Box>
      );
    }
    return null;
  };

  return (
    <Box flex={1} content="center" items="center">
      {!list.length ? (
        <TouchableOpacity
          style={styles.generateButton}
          onPress={onPressGenerate}>
          {loading ? (
            <ActivityIndicator size="large" color="#fff" />
          ) : (
            <Text
              fontSize={18}
              fontWeight="600"
              textTransform="capitalize"
              color="#fff">
              display random photos
            </Text>
          )}
        </TouchableOpacity>
      ) : (
        <FlatList
          data={list}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          style={styles.listContainer}
          contentContainerStyle={styles.listContentContainer}
          columnWrapperStyle={styles.columnWrapperStyle}
          ItemSeparatorComponent={() => <Spacer />}
          keyExtractor={(_, index) => index.toString()}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              colors={['#222850']}
              onRefresh={onRefresh}
            />
          }
          renderItem={renderItem}
          onEndReached={onEndReached}
          ListFooterComponent={renderFooter}
          onEndReachedThreshold={0.1}
        />
      )}
    </Box>
  );
};

export default memo(Home);

const styles = StyleSheet.create({
  generateButton: {
    backgroundColor: '#222850',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: 20,
  },
  listContainer: {
    width: '100%',
  },
  listContentContainer: {
    paddingVertical: 20,
  },
  image: {
    borderRadius: 14,
  },
  columnWrapperStyle: {
    justifyContent: 'space-evenly',
  },
});
