import {RouteProp} from '@react-navigation/core';
import {
  AppNavigatorParamList,
  AppNavigatorProps,
} from '../../Navigations/AppNavigator/Types';

export type ProductDetailsNavigatorProps = {
  navigation: AppNavigatorProps;
  route: RouteProp<AppNavigatorParamList, 'ProductDetails'>;
};
