import {memo} from 'react';
import {StyleSheet, useWindowDimensions} from 'react-native';
import Animated from 'react-native-reanimated';
// Components
import {Box} from '../../Components/Commons';
// Types
import {ProductDetailsNavigatorProps} from './Types';
// Utils
import {sharedTransactions} from '../../Utils/CustomAnimations';

const ProductDetails: React.FC<ProductDetailsNavigatorProps> = props => {
  const {route} = props;
  const {user} = route.params;
  const {width} = useWindowDimensions();

  return (
    <Box>
      <Animated.Image
        sharedTransitionTag={user.id}
        // sharedTransitionStyle={sharedTransactions}
        source={{uri: user.picture}}
        style={[styles.image, {width: width, height: width}]}
      />
    </Box>
  );
};

export default memo(ProductDetails);

const styles = StyleSheet.create({
  image: {borderRadius: 14},
});
