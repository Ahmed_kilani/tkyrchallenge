import {NavigatorScreenParams} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {AppNavigatorParamList} from '../AppNavigator/Types';

export type MainNavigatorParamList = {
  AppNavigator: NavigatorScreenParams<AppNavigatorParamList>;
  OfflineMode: undefined;
};
//
export type MainNavigatorNavigationProps =
  NativeStackNavigationProp<MainNavigatorParamList>;
