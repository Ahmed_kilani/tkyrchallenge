import {useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//ContextAPI
import {GlobalParamsContext} from '../../ContextApi';
//Navigator
import AppNavigator from '../AppNavigator';
import Splash from '../../Screens/Splash';
import OfflineMode from '../../Screens/OfflineMode';
//Types
import {MainNavigatorParamList} from './Types';

const Stack = createNativeStackNavigator<MainNavigatorParamList>();

export default function () {
  const {loadingApp, serverIsDown, isConnected} =
    useContext(GlobalParamsContext);

  //render
  const renderNavigator = () => {
    if (serverIsDown || !isConnected) {
      return <Stack.Screen name="OfflineMode" component={OfflineMode} />;
    }
    return <Stack.Screen name="AppNavigator" component={AppNavigator} />;
  };
  if (loadingApp) {
    return <Splash />;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {renderNavigator()}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
