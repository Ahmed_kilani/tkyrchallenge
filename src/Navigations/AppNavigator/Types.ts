import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {UsersType} from '../../Screens/Home/Types';

type ProductDetailsParamList = {
  user: UsersType;
};

export type AppNavigatorParamList = {
  Home: undefined;
  ProductDetails: ProductDetailsParamList;
};
export type AppNavigatorProps =
  NativeStackNavigationProp<AppNavigatorParamList>;
