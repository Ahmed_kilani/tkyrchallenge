import {Platform, StyleSheet} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//Constants
import {Images} from '../../Constants';
//Screens
import Home from '../../Screens/Home';
import ProductDetails from '../../Screens/ProductDetails';
//types
import {AppNavigatorParamList} from './Types';

const Stack = createNativeStackNavigator<AppNavigatorParamList>();

const AppNavigator: React.FC = () => {
  //render
  const renderLogo = () => {
    return <Images.Logo height={44} />;
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerTitleAlign: 'center',
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerTitle: renderLogo, headerStyle: styles.headerStyle}}
      />
      <Stack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={{
          headerTransparent: true,
          headerTitle: '',
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#222850',
    ...Platform.select({
      ios: {
        height: 100,
      },
      android: {
        height: 75,
      },
    }),
  },
});

export default AppNavigator;
