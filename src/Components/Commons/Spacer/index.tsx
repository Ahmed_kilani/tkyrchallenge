import {memo} from 'react';
//components
import {Box} from '../Box';
//Types
import {SpacerProps} from './Types';

const Item: React.FC<SpacerProps> = props => {
  const {size} = props;
  return <Box width={size || 16} height={size || 16} />;
};

export const Spacer = memo(Item);
