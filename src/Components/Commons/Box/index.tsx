import {memo} from 'react';
import {StyleSheet, View} from 'react-native';
//Types
import {BoxProps} from './Types';

const Item: React.FC<BoxProps> = props => {
  const {
    children,
    style,
    shadow,
    flex,
    direction,
    alignSelf,
    height,
    maxHeight,
    minHeight,
    overflow,
    width,
    maxWidth,
    minWidth,
    content,
    items,
    flexWrap,
    padding,
    pTop,
    pBottom,
    pRight,
    pLeft,
    margin,
    mTop,
    mBottom,
    mRight,
    mLeft,
    background,
    bRadius,
    btlRadius,
    btrRadius,
    bblRadius,
    bbrRadius,
    bWidth,
    btWidth,
    brWidth,
    bbWidth,
    blWidth,
    bColor,
    btColor,
    brColor,
    bbColor,
    blColor,
    position,
    zIndex,
    top,
    bottom,
    left,
    right,
    pointerEvents,
    ...rest
  } = props;
  //useEffect

  return (
    <View
      pointerEvents={pointerEvents}
      style={[
        styles.box,
        style,
        {flexDirection: direction},
        shadow && styles.shadow,
        alignSelf && {alignSelf},
        bColor && {borderColor: bColor},
        btColor && {borderTopColor: btColor},
        brColor && {borderRightColor: btColor},
        bbColor && {borderBottomColor: bbColor},
        blColor && {borderLeftColor: blColor},
        background && {backgroundColor: background},
        content && {justifyContent: content},
        items && {alignItems: items},
        position && {position},
        flexWrap && {flexWrap},
        overflow && {overflow},
        typeof flex === 'number' && {flex},
        typeof height === 'number' && {height},
        typeof maxHeight === 'number' && {maxHeight},
        typeof minHeight === 'number' && {minHeight},
        typeof width === 'number' && {width},
        typeof minWidth === 'number' && {minWidth},
        typeof maxWidth === 'number' && {maxWidth},
        typeof bRadius === 'number' && {borderRadius: bRadius},
        typeof btlRadius === 'number' && {borderTopLeftRadius: btlRadius},
        typeof btrRadius === 'number' && {borderTopRightRadius: btrRadius},
        typeof bblRadius === 'number' && {borderBottomLeftRadius: bblRadius},
        typeof bbrRadius === 'number' && {borderBottomRightRadius: bbrRadius},
        typeof bWidth === 'number' && {borderWidth: bWidth},
        typeof brWidth === 'number' && {borderRightWidth: brWidth},
        typeof btWidth === 'number' && {borderTopWidth: btWidth},
        typeof bbWidth === 'number' && {borderBottomWidth: bbWidth},
        typeof blWidth === 'number' && {borderLeftWidth: blWidth},
        typeof padding === 'number' && {padding},
        typeof pTop === 'number' && {paddingTop: pTop},
        typeof pBottom === 'number' && {paddingBottom: pBottom},
        typeof pRight === 'number' && {paddingRight: pRight},
        typeof pLeft === 'number' && {paddingLeft: pLeft},
        typeof margin === 'number' && {margin},
        typeof mTop === 'number' && {marginTop: mTop},
        typeof mBottom === 'number' && {marginBottom: mBottom},
        typeof mRight === 'number' && {marginRight: mRight},
        typeof mLeft === 'number' && {marginLeft: mLeft},
        typeof zIndex === 'number' && {zIndex},
        typeof top === 'number' && {top},
        typeof bottom === 'number' && {bottom},
        typeof right === 'number' && {right},
        typeof left === 'number' && {left},
      ]}
      {...rest}>
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  box: {},
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
});

export const Box = memo(Item);
