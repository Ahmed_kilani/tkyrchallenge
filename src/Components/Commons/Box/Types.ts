import {FlexAlignType, StyleProp, ViewStyle} from 'react-native';

export type FlexDirectionProps =
  | 'row'
  | 'column'
  | 'row-reverse'
  | 'column-reverse';
export type BoxProps = {
  shadow?: boolean;
  flex?: number;
  direction?: FlexDirectionProps;
  content?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  items?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline';
  flexWrap?: 'wrap' | 'nowrap' | 'wrap-reverse';
  alignSelf?: 'auto' | FlexAlignType;
  padding?: number;
  pTop?: number;
  pBottom?: number;
  pRight?: number;
  pLeft?: number;
  margin?: number;
  mTop?: number;
  mBottom?: number;
  mRight?: number;
  mLeft?: number;
  background?: string;
  bRadius?: number;
  btlRadius?: number;
  btrRadius?: number;
  bblRadius?: number;
  bbrRadius?: number;
  height?: number;
  maxHeight?: number;
  minHeight?: number;
  width?: number;
  minWidth?: number;
  maxWidth?: number;
  bWidth?: number;
  btWidth?: number;
  brWidth?: number;
  bbWidth?: number;
  blWidth?: number;
  bColor?: string;
  btColor?: string;
  brColor?: string;
  bbColor?: string;
  blColor?: string;
  position?: 'absolute' | 'relative';
  zIndex?: number;
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
  style?: StyleProp<ViewStyle>;
  children?: React.ReactNode;
  overflow?: 'visible' | 'hidden' | 'scroll';
  pointerEvents?: 'box-none' | 'none' | 'box-only' | 'auto';
};
