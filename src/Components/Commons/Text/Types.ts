import React from 'react';
import {TextProps as TextElementProps} from 'react-native';

export type TextAlignProps = 'auto' | 'left' | 'right' | 'center' | 'justify';

export type TextProps = TextElementProps & {
  primary?: boolean;
  secondary?: boolean;
  accent?: boolean;
  textAlign?: TextAlignProps;
  fontSize?: number;
  color?: string | null;
  margin?: number;
  mTop?: number;
  mBottom?: number;
  mLeft?: number;
  mRight?: number;
  lineHeight?: number;
  textTransform?: 'none' | 'capitalize' | 'uppercase' | 'lowercase';
  fontWeight?:
    | 'normal'
    | 'bold'
    | '100'
    | '200'
    | '300'
    | '400'
    | '500'
    | '600'
    | '700'
    | '800'
    | '900';
  textDecorationLine?:
    | 'none'
    | 'underline'
    | 'line-through'
    | 'underline line-through';
  children?: React.ReactNode;
};
