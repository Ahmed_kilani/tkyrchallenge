import {memo} from 'react';
import {Platform, StyleSheet, Text as TextElement} from 'react-native';
//Types
import {TextProps, TextAlignProps} from './Types';

const Item: React.FC<TextProps> = props => {
  const {
    children,
    style,
    primary,
    secondary,
    accent,
    fontSize,
    color,
    margin,
    mTop,
    mBottom,
    mLeft,
    mRight,
    fontWeight,
    textAlign,
    textTransform,
    textDecorationLine,
    lineHeight,
    ...rest
  } = props;

  let labelAlign: TextAlignProps = 'left';
  if (textAlign) {
    labelAlign = textAlign;
  }
  if (!children || children.toString().trim().length === 0) {
    return null;
  }
  let fontFamily;
  if (Platform.OS === 'android' && fontWeight) {
    switch (fontWeight) {
      case '100':
        fontFamily = 'Poppins-ExtraLight';
        break;
      case '200':
        fontFamily = 'Poppins-ExtraLight';
        break;
      case '300':
        fontFamily = 'Poppins-Light';
        break;
      case '400':
        fontFamily = 'Poppins-Regular';
        break;
      case '500':
        fontFamily = 'Poppins-Medium';
        break;
      case '600':
        fontFamily = 'Poppins-Medium';
        break;
      case '700':
        fontFamily = 'Poppins-SemiBold';
        break;
      case '800':
        fontFamily = 'Poppins-SemiBold';
        break;
      case '900':
        fontFamily = 'Poppins-Bold';
        break;
      default:
        break;
    }
  }
  return (
    <TextElement
      allowFontScaling={false}
      style={[
        style,
        styles.text,
        color && {color},
        {textAlign: labelAlign},
        primary && styles.primary,
        secondary && styles.secondary,
        accent && styles.accent,
        fontFamily && {fontFamily},
        typeof fontSize === 'number' && {fontSize},
        typeof margin === 'number' && {margin},
        typeof mTop === 'number' && {marginTop: mTop},
        typeof mBottom === 'number' && {marginBottom: mBottom},
        typeof mLeft === 'number' && {marginLeft: mLeft},
        typeof mRight === 'number' && {marginRight: mRight},
        typeof lineHeight === 'number' && {lineHeight},
        fontWeight && {fontWeight},
        textTransform && {textTransform},
        textDecorationLine && {textDecorationLine},
      ]}
      {...rest}>
      {children}
    </TextElement>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    color: '#000',
  },
  primary: {
    fontFamily: 'Poppins-Regular',
  },
  secondary: {},
  accent: {},
});

export const Text = memo(Item);
