/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {StatusBar, StyleSheet} from 'react-native';

import {ProviderGlobalParams} from './src/ContextApi';
import MainNavigator from './src/Navigations/MainNavigator';

export default function App() {
  return (
    <GestureHandlerRootView style={styles.main}>
      <ProviderGlobalParams>
        <>
          <StatusBar barStyle="dark-content" backgroundColor="#F8F8F8" />
          <MainNavigator />
        </>
      </ProviderGlobalParams>
    </GestureHandlerRootView>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
});
